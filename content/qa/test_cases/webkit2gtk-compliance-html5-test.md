+++
date = "2016-06-09"
weight = 100

title = "webkit2gtk-compliance-html5-test"

aliases = [
    "/old-wiki/QA/Test_Cases/webkit2GTK-compliance-HTML5-test"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/

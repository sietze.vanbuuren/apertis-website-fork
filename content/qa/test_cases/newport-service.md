+++
date = "2016-12-14"
weight = 100

title = "newport-service"

aliases = [
    "/old-wiki/QA/Test_Cases/newport-service"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/

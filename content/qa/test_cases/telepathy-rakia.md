+++
date = "2017-10-25"
weight = 100

title = "telepathy-rakia"

aliases = [
    "/old-wiki/QA/Test_Cases/telepathy-rakia"
]
+++
This test case has now been made obsolete. Current test definitions are now available at https://qa.apertis.org/

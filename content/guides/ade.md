+++
date = "2018-10-17"
lastmod = "2021-09-15"
weight = 100
toc = true

title = "Apertis Development Environment"

aliases = [
    "/old-developer/latest/appdev-ade.html",
    "/old-developer/v2019/appdev-ade.html",
    "/old-developer/v2020/appdev-ade.html",
    "/old-developer/v2021pre/appdev-ade.html",
    "/old-developer/v2022dev0/appdev-ade.html",
    "/old-wiki/SDK"
]
+++

Apertis provides tools which help with the building of applications targeting a
hardware platform; the installation of development versions of the application
to the target device and debugging of the application.

These tools are pre-installed in the Apertis SDK. The SDK images shipped by
Apertis are meant to be run in [VirtualBox]( {{< ref "/virtualbox.md" >}} ).
They provide a common, reproducible environment to build both platform
packages and application-bundles for Apertis.  The tool provides general manual
pages so you can get command information by using `man ade` on the SDK.

# Sample applications

The Apertis SDK provides sample applications in the `user` home folder.  Each
application aims to demonstrate how to create a bundle addressing a minimal use
case.

* [helloworld-app](https://gitlab.apertis.org/sample-applications/helloworld-app):
  A basic sample application which can be used as the basic skeleton for more
  complex application
* [helloworld-simple-agent](https://gitlab.apertis.org/sample-applications/helloworld-simple-agent):
  An agent-only application which does not provide any graphical program
* [helloworld-agentapp](https://gitlab.apertis.org/sample-applications/helloworld-agentapp):
  An agent application which interacts with a graphical program
* [helloworld-https-client](https://gitlab.apertis.org/sample-applications/helloworld-https-client):
  A basic HTTP(S) client application
* [helloworld-persistapp](https://gitlab.apertis.org/sample-applications/helloworld-persistapp):
  An application to deal with persistent data
* [helloworld-prefsapp](https://gitlab.apertis.org/sample-applications/helloworld-prefsapp):
  An application to deal with preferences
* [notification-example](https://gitlab.apertis.org/sample-applications/notification-example):
  An example for using Notifications API
* [hard-keys-example](https://gitlab.apertis.org/sample-applications/hard-keys-example):
  An example for using hardware keys
* [cpp-library-example](https://gitlab.apertis.org/sample-applications/cpp-library-example):
  An example for using external library with C++
* [helloworld-webapp](https://gitlab.apertis.org/sample-applications/helloworld-webapp):
  A basic Web Runtime application

{{% notice warning %}}
The custom application framework based on the Canterbury app manager has been
removed from `v2022dev2` in favor of upstream components like Flatpak, see the
[application framework]( {{< ref "application-framework.md" >}} ) document for
more details.
{{% /notice %}}

{{% notice info %}}
The sample applications are also available from the
[Apertis gitlab](https://gitlab.apertis.org/sample-applications).
{{% /notice %}}

To demonstrate how to use `ade`,
[helloworld-app](https://gitlab.apertis.org/sample-applications/helloworld-app)
is going to be used through this document.

# Building and running an application bundle with ade on the SDK

To install and test on the SDK, `ade` provides the `--native` option.

    $ cd /home/user/sample-applications/helloworld-app
    $ ade configure --native
    $ ade build --native
    $ ade export
    $ ade install --native
    $ ade run --native

If you want to build the application in debug mode, you should use `ade configure --native --debug`.
When the build is done, `ade export` will create a bundle file. For the `helloworld-app` example,
the command will generate the `org.apertis.HelloWorldApp-0.1.0.bundle` file under the current path.

The final step for installation is to run `ade install --native`. Then, if it is done successfully,
you can run the application by executing `ade run --native`.

# Building and debugging app bundles with ade

The following steps can be used to cross-compile an example "Hello World"
agent, deploy it onto the target device and debug it remotely using `gdb` using
the Apertis helper tool called `ade`:


# Debugging on a target device with ADE

## Installation of a sysroot

To run/debug an application bundle on a target device, the first step is to download a sysroot.
You can manage sysroots on the SDK with the `ade sysroot` command. See `man ade-sysroot` for more
details about sysroot-related options.

`ade sysroot list` is used to check which sysroots are already installed
on the SDK.  If there's no installed sysroot image, the result would be like the following
message.


    $ ade sysroot list
    No sysroot installed in directory /opt/sysroot/.

`ade sysroot latest` provides the version and downloadable URL of the sysroot image.

    $ ade sysroot latest
    * No distribution specified, defaulting to host distribution
    * No release version specified, defaulting to host release version
    * No architecture specified, defaulting to 'armhf'
    * Checking latest version available for apertis - v2021 (armhf)
    * Retrieved latest version: apertis v2021 - 20210915.0115 (armhf)
    * Download URL: https://images.apertis.org/daily/v2021/20210915.0115/armhf/sysroot/sysroot-apertis-v2021-armhf-20210915.0115.tar.gz

The `ade sysroot install` command will do everything needed to install the
latest sysroot version on your SDK, from downloading to installing without any
manual intervention.  This will default to installing a sysroot for `armhf`,
using the distribution and release of the current SDK, specify using the
command line arguments if this is not what is required (see `ade sysroot
install --help` for more information):

    $ ade sysroot install
    * No distribution specified, defaulting to host distribution
    * No release version specified, defaulting to host release version
    * No architecture specified, defaulting to 'armhf'
    * Installing version apertis v2021 - 20210915.0115 (armhf)
    sysroot.tar.gz |==================================================| 100%

When the installation command has completed its job, the installed status can be checked with `ade sysroot installed`.

    $ ade sysroot installed
    * No distribution specified, defaulting to host distribution
    * No release version specified, defaulting to host release version
    * No architecture specified, defaulting to 'armhf'
    * Retrieved current version: apertis v2021 - 20210915.0115 (armhf)

## Setting up for a target device

For debugging on a target device, the target should accept remote connections via SSH and
allow a specific port for remote gdb connection.


### Preparing SSH key-pair on the SDK

SSH key is required to allow connecting from SDK to the target without
requesting password.  The following commands will generate a SSH RSA key-pair
(using the legacy PEM format) and copy the generated public key to the target.
In the example, it assumes that the address of the target device is
`192.168.0.100`. Accept the default settings so as not to require any password
to access the target board.

    $ export TARGET_HOST=192.168.0.100
    $ ssh-keygen -t rsa -m PEM
    Generating public/private rsa key pair.
    Enter file in which to save the key (/home/user/.ssh/id_rsa):
    Created directory '/home/user/.ssh'.
    Enter passphrase (empty for no passphrase):
    Enter same passphrase again:
    Your identification has been saved in /home/user/.ssh/id_rsa.
    Your public key has been saved in /home/user/.ssh/id_rsa.pub.
    The key fingerprint is:
    SHA256:Yd3+gA4ZpWUczdlFKFlaNsyQfET09+UJF5LZYNAaFrI user@apertis
    The key's randomart image is:
    +---[RSA 2048]----+
    |         o=B=^&=o|
    |         *++XBB+.|
    |        =Eo =+ .+|
    |       . + +  o.=|
    |        S . o  .o|
    |         o   o   |
    |          .   .  |
    |                 |
    |                 |
    +----[SHA256]-----+
    $ ssh-copy-id user@$TARGET_HOST

### Installing GDB on a target device

Before starting to debug of target, we need to ensure that `gdbserver` is
installed on the remote device:

    ssh user@$TARGET_HOST "sudo apertis-dev"
    ssh user@$TARGET_HOST "sudo apt update && sudo apt install gdbserver"

### Using .gdbinit

For debugging, we are going to use GDB remote connection so the target application will be started as soon as
executing `ade` debug command on the SDK. During GDB startup, the minimal information such as the location of
sysroot, and debugging symbols, should be provided by `.gdbinit`.

    $ cat > ~/.gdbinit <<EOF
    set sysroot /opt/sysroot/apertis/v2021/armhf
    set debug-file-directory /opt/sysroot/apertis/v2021/armhf/usr/lib/debug
    EOF

### Adding debug symbols for packages needed by the application

{{% notice note %}}
This section will become obsolete and will be dropped once https://phabricator.apertis.org/T3819 is fixed.
{{% /notice %}}

To get proper backtraces under GDB you need debug symbols for all the libaries in the stack used by your application.
The current sysroots do not ship the debug symbols for every installed package and a manual step is needed to fetch them.

From the SDK, the following commands will install the debug symbol packages (`libglib2.0-0-dbgsym`, `libclutter-1.0-0-dbgsym`,
`libgtk-3-0-dbgsym`, `libmildenhall-0-0-dbgsym`) on the installed sysroot image.

    $ bwrap --bind /opt/sysroot/apertis/v2021/armhf/ / \
            --proc /proc \
            --dev-bind /dev /dev \
            --setenv PATH /sbin:/usr/sbin:/usr/local/sbin:$PATH \
            fakeroot apt update
    
    $ bwrap --bind /opt/sysroot/apertis/v2021/armhf/ / \
            --proc /proc \
            --dev-bind /dev /dev \
            --setenv PATH /sbin:/usr/sbin:/usr/local/sbin:$PATH \
            fakeroot apt install libglib2.0-0-dbgsym libclutter-1.0-0-dbgsym libgtk-3-0-dbgsym libmildenhall-0-0-dbgsym

## Debugging on a target board

Debugging an application on a target device is similar to running on the SDK.
For the following example steps, we assume that the desired sysroot has been already installed
on the SDK, that you are using a SSH key-pair to login to your target device, and that the address of
the device is mapped to `TARGET_HOST` environment variable.

To build an application bundle for your target device in debug mode, the following `ade` commands will
be used.

    $ ade configure --debug --device user@$TARGET_HOST
    $ ade build --debug --device user@$TARGET_HOST
    $ ade install --device user@$TARGET_HOST

For debugging, `ade` provides `debug` command for a target. As we mentioned above, it requires to allow
connecting to GDB remote port (default:1234). Then, the command will bring you to GDB remote prompt.

    $ ade debug --device user@$TARGET_HOST

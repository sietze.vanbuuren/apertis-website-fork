+++
date = "2020-12-18"
weight = 100

title = "v2022dev0 Release Notes"
+++

Apertis is a Debian derivative distribution geared towards the creation
of product-specific images for ARM (both the 32bit ARMv7 and 64-bit
ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

**Apertis v2022dev0** is the first **development** release of the Apertis
v2022 stable release flow that will lead to the LTS **Apertis v2022.0**
release in March 2022.

This Apertis release is built on top of Debian Buster with several
customisations and the Linux kernel 5.9.x series until the 5.10.x LTS series
is released.

Test results for the v2022dev0 release are available in the following
test reports:

  - [APT images](https://lavaphabbridge.apertis.org/report/v2022dev0/20201209.0116)
  - [OSTree images](https://lavaphabbridge.apertis.org/report/v2022dev0/20201209.0116/ostree)
  - [NFS artifacts](https://lavaphabbridge.apertis.org/report/v2022dev0/20201209.0116/nfs)
  - [LXC containers](https://lavaphabbridge.apertis.org/report/v2022dev0/20201209.0116/lxc)

## Release flow

  - **2020 Q4: v2022dev0**
  - 2021 Q1: v2022dev1
  - 2021 Q2: v2022dev2
  - 2021 Q3: v2022dev3
  - 2021 Q4: v2022pre
  - 2022 Q1: v2022.0
  - 2022 Q2: v2022.1
  - 2022 Q3: v2022.2
  - 2022 Q4: v2022.3
  - 2023 Q1: v2022.4
  - 2023 Q2: v2022.5
  - 2023 Q3: v2022.6
  - 2023 Q4: v2022.7

### Release downloads

| [Apertis v2022dev0 images](https://images.apertis.org/release/v2022dev0/) | | | | |
| ------------------------------------------------------------------------- |-|-|-|-|
| Intel 64-bit		| [minimal](https://images.apertis.org/release/v2022dev0/v2022dev0.0/amd64/minimal/apertis_v2022dev0-minimal-amd64-uefi_v2022dev0.0.img.gz) | [target](https://images.apertis.org/release/v2022dev0/v2022dev0.0/amd64/target/apertis_v2022dev0-target-amd64-uefi_v2022dev0.0.img.gz) | [base SDK](https://images.apertis.org/release/v2022dev0/v2022dev0.0/amd64/basesdk/apertis_v2022dev0-basesdk-amd64-sdk_v2022dev0.0.vdi.gz) | [SDK](https://images.apertis.org/release/v2022dev0/v2022dev0.0/amd64/sdk/apertis_v2022dev0-sdk-amd64-sdk_v2022dev0.0.vdi.gz)
| ARM 32-bit (U-Boot)	| [minimal](https://images.apertis.org/release/v2022dev0/v2022dev0.0/armhf/minimal/apertis_v2022dev0-minimal-armhf-uboot_v2022dev0.0.img.gz) | [target](https://images.apertis.org/release/v2022dev0/v2022dev0.0/armhf/target/apertis_v2022dev0-target-armhf-uboot_v2022dev0.0.img.gz)
| ARM 64-bit (U-Boot)	| [minimal](https://images.apertis.org/release/v2022dev0/v2022dev0.0/arm64/minimal/apertis_v2022dev0-minimal-arm64-uboot_v2022dev0.0.img.gz)
| ARM 64-bit (Raspberry Pi)	| [minimal](https://images.apertis.org/release/v2022dev0/v2022dev0.0/arm64/minimal/apertis_v2022dev0-minimal-arm64-rpi64_v2022dev0.0.img.gz) | [target](https://images.apertis.org/release/v2022dev0/v2022dev0.0/arm64/target/apertis_v2022dev0-target-arm64-rpi64_v2022dev0.0.img.gz)

The Intel `minimal` and `target` images are tested on the
[reference hardware (MinnowBoard Turbot Dual-Core)]( {{< ref "/reference_hardware/amd64.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis v2022dev0 repositories

    deb https://repositories.apertis.org/apertis/ v2022dev0 target development sdk hmi

## Changes

This release begins the journey that will lead to the v2022 stable release in March 2022.

The large part of the new changes in this quarter have also been landed in the
[v2021pre release]({{< ref "release/v2021pre/releasenotes" >}}), with the
[v2021pre release notes]({{< ref "release/v2021pre/releasenotes" >}})
describing them in detail.

### Dynamic KVM/UML backend selection in Debos

During the v2021dev1 release cycle an
[alternative backend for the Debos image builder]({{< ref "release/v2021dev1/releasenotes.md#gitlabci-based-image-building-pipeline-technology-preview" >}})
using UML rather than KVM was developed to make our build pipeline more
cloud-friendly, since KVM is often not available on machines provided by
cloud providers. Unfortunately UML is by design quite slower than
hardware-assisted KVM and this introduced a performance regression for
developers using machines with KVM virtualization available.

In this cycle the UML backend was landed to the upstream main branch, adding
automatic backend selection: Debos is now able to use KVM if available and fall
back to UML in other cases without any user intervention, giving the best
performance available.

## Deprecations and ABI/API breaks

### Regressions

#### Loop device issues with kernel 5.9

Due to the lateness of the 5.10 LTS release, this release still ships version
5.9 of the Linux kernel, which has shown a few regressions with loop devices
impacting the ability to use offline bundles for the Apertis update manager.

Experiments with the 5.10 release candidates seem to indicate that the issues
have been addressed, and a minor update to the v2022dev0 release is thus
planned in early January to ship the 5.10 kernel once it is released.

#### Mildenhall HMI flickering

Enabling hardware acceleration for the graphical stack on the i.MX6 images
is now causing visual glitches in the Mildenhall HMI while the AGL compositor
is not affected.

As a workaround it is possible to disable hardware acceleration when using
the Mildenhall compositor:

    sudo sed -i -e 's|Exec=/usr/bin/mildenhall|Exec=/usr/bin/env GBM_ALWAYS_SOFTWARE=1 /usr/bin/mildenhall|' /etc/xdg/user-session-launcher/session.launcher

#### OSTree offline update bundle format change

The upstream `libostree` 2020.7 version supports two different formats for
static delta bundles, as used for offline updates: the old one without signed
metadata and a new one which also signs the superblock metadata to ensure it
can be trusted.

Support for signed metadata was first introduced in Apertis with the v2021dev2
release and was [later upstreamed](https://github.com/ostreedev/ostree/pull/1985).

However, during the upstreaming process the format has changed and Apertis now
aligns to the upstream format.

This means that signed bundles created with the OSTree version from the v2021
development releases won't be accepted by this release.

#### Missing support in the updater for the new OSTree signed offline update format

This release ships with a new `libostree` upstream version supporting signed
metadata for offline bundles, but the Apertis Update Manager (AUM) lacks a few
fixes to accept signed offline update bundles generated with it.

An update is planned in early January to fix this issue.

### Deprecations

During this release cycle we have continued to mark obsolete or
problematic APIs with the
[ABI break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

### Breaks

No known breaking changes are part of this release.

## Infrastructure

### Apertis Docker images

The Apertis Docker images provide a unified and easily reproducible build
environment for developers and services.

As of today, this includes the
[`apertis-base`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev0-base),
[`apertis-image-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-images/v2022dev0-image-builder),
[`apertis-package-source-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-package-sources/v2022dev0-package-source-builder),
[`apertis-flatdeb-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-flatdebs/v2022dev0-flatdeb-builder),
[`apertis-documentation-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-documentations/v2022dev0-documentation-builder),
and [`apertis-testcases-builder`](docker://registry.gitlab.apertis.org/infrastructure/apertis-docker-testcasess/v2022dev0-testcases-builder),
Docker images.

### Apertis infrastructure tools

The [Apertis v2022 infrastructure
repository](https://build.collabora.co.uk/project/users/apertis:infrastructure:v2022)
provides packages for the required versions of `ostree-push` and
`ostree` for Debian Buster:

    deb https://repositories.apertis.org/infrastructure-v2022/ buster infrastructure

### Images

Image daily builds, as well as release builds can be found at https://images.apertis.org/

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The
[Image build infrastructure document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

### High (3)
 - [T7506](https://phabricator.apertis.org/T7506)	non-working upgrade with encrypted bundle with "(kernel 5.9)"
 - [T7555](https://phabricator.apertis.org/T7555)	ImportError: No module named gobject when executing the connman-usb-tethering test
 - [T7557](https://phabricator.apertis.org/T7557)	Mildenhall "flickering" in case if HW EGL support is enabled

### Normal (78)
 - [T2896](https://phabricator.apertis.org/T2896)	Crash when initialising egl on ARM target
 - [T2930](https://phabricator.apertis.org/T2930)	Develop test case for out of screen events in Wayland images
 - [T3210](https://phabricator.apertis.org/T3210)	Fix Tracker testcase to not download media files from random HTTP user folders
 - [T3233](https://phabricator.apertis.org/T3233)	Ribchester: deadlock when calling RemoveApp() right after RollBack()
 - [T3321](https://phabricator.apertis.org/T3321)	libgles2-vivante-dev is not installable
 - [T3920](https://phabricator.apertis.org/T3920)	arm-linux-gnueabihf-pkg-config does not work with sysroots installed by `ade`
 - [T4092](https://phabricator.apertis.org/T4092)	Containers fail to load on Gen4 host
 - [T4293](https://phabricator.apertis.org/T4293)	Preseed action is needed for Debos
 - [T4307](https://phabricator.apertis.org/T4307)	ribchester-core causes apparmor denies on non-btrfs minimal image
 - [T4422](https://phabricator.apertis.org/T4422)	do-branching fails at a late stage cloning OBS binary repos
 - [T4444](https://phabricator.apertis.org/T4444)	A 2-3 second lag between the speakers is observed when a hfp connection is made over bluetooth
 - [T4568](https://phabricator.apertis.org/T4568)	Ribchester mount unit depends on Btrfs
 - [T4660](https://phabricator.apertis.org/T4660)	Eclipse Build is not working for HelloWorld App
 - [T4693](https://phabricator.apertis.org/T4693)	Not able to create namespace for AppArmor container on the internal mx6qsabrelite images with proprietary kernel
 - [T5468](https://phabricator.apertis.org/T5468)	build-snapshot: allow to build packages without `autogen.sh` script
 - [T5487](https://phabricator.apertis.org/T5487)	Wi-Fi search button is missing in wifi application
 - [T5747](https://phabricator.apertis.org/T5747)	The /boot mountpoint is not empty
 - [T5748](https://phabricator.apertis.org/T5748)	System users are shipped in /usr/etc/passwd instead of /lib/passwd
 - [T5863](https://phabricator.apertis.org/T5863)	Songs/Videos don't play on i.MX6 with Frampton on internal images
 - [T5896](https://phabricator.apertis.org/T5896)	sdk-dbus-tools-bustle testcase is failing
 - [T5897](https://phabricator.apertis.org/T5897)	apparmor-ofono test fails
 - [T5900](https://phabricator.apertis.org/T5900)	evolution-sync-bluetooth test fails
 - [T5901](https://phabricator.apertis.org/T5901)	eclipse-plugins-apertis-management package is missing
 - [T5906](https://phabricator.apertis.org/T5906)	Video does not stream in WebKit on the i.MX6 internal images
 - [T5931](https://phabricator.apertis.org/T5931)	connman-usb-tethering test fails
 - [T5993](https://phabricator.apertis.org/T5993)	rhosydd: 8_rhosydd test failed
 - [T6001](https://phabricator.apertis.org/T6001)	eclipse-plugins-remote-debugging test fails
 - [T6008](https://phabricator.apertis.org/T6008)	The pacrunner package used for proxy autoconfiguration is not available
 - [T6024](https://phabricator.apertis.org/T6024)	folks-inspect: command not found
 - [T6052](https://phabricator.apertis.org/T6052)	Multimedia playback is broken on the internal i.MX6 images (internal 3.14 ADIT kernel issue)
 - [T6077](https://phabricator.apertis.org/T6077)	youtube Videos are not playing on upstream webkit2GTK
 - [T6078](https://phabricator.apertis.org/T6078)	Page scroll is lagging in Minibrowser on upstream webkit2GTK
 - [T6111](https://phabricator.apertis.org/T6111)	traprain: 7_traprain test failed
 - [T6231](https://phabricator.apertis.org/T6231)	gitlab-to-obs: Handle packages changing component across releases
 - [T6243](https://phabricator.apertis.org/T6243)	AppArmor ubercache support is no longer enabled after 18.12
 - [T6291](https://phabricator.apertis.org/T6291)	Generated lavaphabbridge error report email provides wrong link for full report link
 - [T6292](https://phabricator.apertis.org/T6292)	gettext-i18n: test failed
 - [T6349](https://phabricator.apertis.org/T6349)	sdk-code-analysis-tools-splint: 3_sdk-code-analysis-tools-splint test failed
 - [T6366](https://phabricator.apertis.org/T6366)	sdk-cross-compilation: 10_sdk-cross-compilation test failed
 - [T6369](https://phabricator.apertis.org/T6369)	apparmor-gstreamer1-0: test failed
 - [T6444](https://phabricator.apertis.org/T6444)	aum-update-rollback-tests/arm64,amd64: Automatic power cut test should be reworked to reduce the speed of delta read
 - [T6446](https://phabricator.apertis.org/T6446)	aum-update-rollback-tests/amd64: DNS not available in LAVA tests after reboot
 - [T6614](https://phabricator.apertis.org/T6614)	aum-update-rollback-tests/armhf: Rollback situation is not reproduced on public armhf target and internal images
 - [T6620](https://phabricator.apertis.org/T6620)	Repeatedly plugging and unplugging a USB flash drive on i.MX6 (Sabrelite) results in USB failure
 - [T6662](https://phabricator.apertis.org/T6662)	SDK: command-not-found package is broken
 - [T6669](https://phabricator.apertis.org/T6669)	Stop building cross compilers tools and libraries for not supported platforms
 - [T6670](https://phabricator.apertis.org/T6670)	Remove or move git-mediawiki package from the development repo
 - [T6727](https://phabricator.apertis.org/T6727)	FTBFS: Apertis v2020pre package build failures
 - [T6768](https://phabricator.apertis.org/T6768)	Fix the kernel command line generation in OSTRee for FIT image
 - [T6773](https://phabricator.apertis.org/T6773)	HAB testing: the unsigned image may pass validation in several circumstances
 - [T6783](https://phabricator.apertis.org/T6783)	Kernel  trace on armhf board with attached screen
 - [T6795](https://phabricator.apertis.org/T6795)	SabreLite failing to boot due to failing "to start udev Coldplug all Devices"
 - [T6806](https://phabricator.apertis.org/T6806)	HAB on SabreLite in open state accepts any signed kernel regardless of the signing key
 - [T6885](https://phabricator.apertis.org/T6885)	gitlab-rulez fails to set location of the gitlab-ci.yaml on first run
 - [T6887](https://phabricator.apertis.org/T6887)	ARM64 target does not reboot automatically
 - [T6903](https://phabricator.apertis.org/T6903)	U-Boot boot counter is used for AMD64 & ARM64
 - [T6961](https://phabricator.apertis.org/T6961)	audio-backhandling feature fails
 - [T7000](https://phabricator.apertis.org/T7000)	DNS resolution does not work in Debos on some setups
 - [T7012](https://phabricator.apertis.org/T7012)	Apparmor Denied session logs keep popping up on the terminal while executing tests
 - [T7016](https://phabricator.apertis.org/T7016)	network proxy for browser application is not resolving on mildenhall-compositor
 - [T7056](https://phabricator.apertis.org/T7056)	FAILED: Error running command: ['ssh-copy-id', 'user@127.0.0.1' errors are shown when running the sdk-persistent testcases
 - [T7127](https://phabricator.apertis.org/T7127)	apparmor-functional-demo:  test failed
 - [T7128](https://phabricator.apertis.org/T7128)	apparmor-session-lockdown-no-deny
 - [T7129](https://phabricator.apertis.org/T7129)	apparmor-tumbler: test failed
 - [T7237](https://phabricator.apertis.org/T7237)	sdk-persistent-disk-sysroot test fails
 - [T7238](https://phabricator.apertis.org/T7238)	sdk persistent memory tests fail
 - [T7247](https://phabricator.apertis.org/T7247)	connman-new-supplicant: test failed
 - [T7308](https://phabricator.apertis.org/T7308)	sdk-vb-fullscreen testcase link needs to be changed for v2019
 - [T7333](https://phabricator.apertis.org/T7333)	apparmor-geoclue: test failed
 - [T7340](https://phabricator.apertis.org/T7340)	newport: test failed
 - [T7503](https://phabricator.apertis.org/T7503)	Failed to unmount /usr on ostree-based images: Device or resource busy log is seen on rebooting
 - [T7511](https://phabricator.apertis.org/T7511)	Syntax highlight on the website is producing undesirable results
 - [T7512](https://phabricator.apertis.org/T7512)	debos sometimes fails to mount things
 - [T7530](https://phabricator.apertis.org/T7530)	ADE can't download amd64 sysroot.
 - [T7531](https://phabricator.apertis.org/T7531)	connman-usb-tethering testcase fails in apertis v2020 and v2019 images
 - [T7559](https://phabricator.apertis.org/T7559)	grilo: test failed
 - [T7567](https://phabricator.apertis.org/T7567)	hawkBit upload jobs are failing
 - [T7569](https://phabricator.apertis.org/T7569)	add-repo: test failed

### Low (15)
 - [T1809](https://phabricator.apertis.org/T1809)	Upstream: linux-tools-generic should depend on lsb-release
 - [T1964](https://phabricator.apertis.org/T1964)	Mildenhall compositor crops windows
 - [T2226](https://phabricator.apertis.org/T2226)	Network search pop-up isn't coming up in wi-fi settings
 - [T2367](https://phabricator.apertis.org/T2367)	Videos are hidden when Eye is launched
 - [T2483](https://phabricator.apertis.org/T2483)	Video doesn't play when toggling from full screen to detail view
 - [T2704](https://phabricator.apertis.org/T2704)	The video player window is split into 2 frames in default view
 - [T3161](https://phabricator.apertis.org/T3161)	If 2 drawers are activated, the most recent one hides behind the older one, instead of coming on top of older one.
 - [T3759](https://phabricator.apertis.org/T3759)	Status bar is not getting updated with the current song/video being played
 - [T4166](https://phabricator.apertis.org/T4166)	On multiple re-entries from settings to eye the compositor hangs
 - [T4296](https://phabricator.apertis.org/T4296)	Segmentation fault is observed on closing the mildenhall-compositor
 - [T7204](https://phabricator.apertis.org/T7204)	Bluez pairing operation is failing from time to time
 - [T7225](https://phabricator.apertis.org/T7225)	webkit2gtk-ac-3d-rendering test fails
 - [T7406](https://phabricator.apertis.org/T7406)	RNG-Hardware-error error logs are seen on booting up the i.MX6 target
 - [T7514](https://phabricator.apertis.org/T7514)	Remote outputs and local output, buffer size combined in Maynard/GTK
 - [T7551](https://phabricator.apertis.org/T7551)	Maynard shows canterbury applications that shouldn't be displayed

### Lowest (28)
 - [T1556](https://phabricator.apertis.org/T1556)	No connectivity Popup is not seen when the internet is disconnected.
 - [T1960](https://phabricator.apertis.org/T1960)	remove INSTALL, aclocal.m4 files from langtoft
 - [T2028](https://phabricator.apertis.org/T2028)	Documentation is not available from the main folder
 - [T2299](https://phabricator.apertis.org/T2299)	Clutter_text_set_text API redraws entire clutterstage
 - [T2475](https://phabricator.apertis.org/T2475)	Theme ,any F node which is a child of an E node is not working for Apertis widgets.
 - [T2833](https://phabricator.apertis.org/T2833)	Interaction with PulseAudio not allowed by its AppArmor profile
 - [T2995](https://phabricator.apertis.org/T2995)	Focus in launcher rollers broken because of copy/paste errors
 - [T3174](https://phabricator.apertis.org/T3174)	Clang package fails to install appropriate egg-info needed by hotdoc
 - [T3219](https://phabricator.apertis.org/T3219)	Canterbury messes up kerning when .desktop uses unicode chars
 - [T3237](https://phabricator.apertis.org/T3237)	make check fails on libbredon package for wayland warnings
 - [T3580](https://phabricator.apertis.org/T3580)	Canterbury entry-point launching hides global popups, but only sometimes
 - [T3588](https://phabricator.apertis.org/T3588)	<abstractions/chaiwala-base> gives privileges that not every app-bundle should have
 - [T3730](https://phabricator.apertis.org/T3730)	canterbury: Most of the tests fail
 - [T3771](https://phabricator.apertis.org/T3771)	Roller problem in settings application
 - [T3797](https://phabricator.apertis.org/T3797)	Variable roller is not working
 - [T3798](https://phabricator.apertis.org/T3798)	In mildenhall, URL history speller implementation is incomplete.
 - [T3909](https://phabricator.apertis.org/T3909)	MildenhallSelectionPopupItem doesn't take ownership when set properties
 - [T3969](https://phabricator.apertis.org/T3969)	MildenhallSelPopupItem model should be changed to accept only gchar * instead of MildenhallSelPopupItemIconDetail for icons
 - [T3971](https://phabricator.apertis.org/T3971)	libbredon/seed uninstallable on target as they depend on libraries in :development
 - [T3991](https://phabricator.apertis.org/T3991)	virtual keyboard is not showing for password input field of any webpage
 - [T3992](https://phabricator.apertis.org/T3992)	Steps like pattern is seen in the background in songs application
 - [T3996](https://phabricator.apertis.org/T3996)	Avoid unconstrained dbus AppArmor rules in frome
 - [T4027](https://phabricator.apertis.org/T4027)	webkit2GTK crash observed flicking on webview from other widget
 - [T4031](https://phabricator.apertis.org/T4031)	Mildenhall should install themes in the standard xdg data dirs
 - [T4050](https://phabricator.apertis.org/T4050)	Render theme buttons are not updating with respect to different zoom levels
 - [T4348](https://phabricator.apertis.org/T4348)	Inital Roller mappings are misaligned on the HMI
 - [T4419](https://phabricator.apertis.org/T4419)	traprain: sadt: error: cannot find debian/tests/control
 - [T4421](https://phabricator.apertis.org/T4421)	ribchester: Job for generated-test-case-ribchester.service canceled

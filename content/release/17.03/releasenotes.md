+++
date = "2017-06-07"
weight = 100

title = "17.03 ReleaseNotes"

aliases = [
    "/old-wiki/17.03/ReleaseNotes"
]
status = "Deprecated"
statusDescription = "This release of Apertis has been deprecated. Links to images and other resources have either been removed or are likely to be dead."
+++

# Apertis 17.03 Release

**17.03** is the current stable development release of **Apertis**, a
Debian/Ubuntu derivative distribution geared towards the creation of
product-specific images for ARM (both in the 32bit ARMv7 version using
the hardfloat ABI and in the 64-bit ARMv8 version) and Intel x86-64
(64-bit) systems.

This Apertis release is based on top of the Ubuntu 16.04 (Xenial) LTS
release with several customization. Test results of the 17.03 release
are available in the [17.03 test report]().

### Release downloads

| [Apertis 17.03 images]() |
| ----------------------------------------------------------------- |
| Intel 64-bit                                                      |

The `minimal`, `target` and `development` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis 17.03 repositories

` $ deb `<https://repositories.apertis.org/apertis/>` 17.03 target helper-libs development sdk hmi`

## New features

### ARM64 support

Apertis gained support for the latest 64-bit version of the ARM
architecture (also known as ARMv8/aarch64), which involved a full
rebuild of the archive and several fixes. No images are available at the
moment since no official 64-bit ARM hardware has been selected, but all
the pre-built packages are available from [the Apertis OBS
instance](https://build.collabora.co.uk/project/show/apertis:17.03:target).

### Headless images with the application framework

The full application framework can now successfully run with no HMI
available, and it's now shipped in the [minimal image
flavour]().
Headless agents can now rely on the application manager to set the
appropriate security profile and manage their lifecycle.

### Simplified automated package testing

Our LAVA-based testing infrastructure has been expanded to run
[DEP-8](http://dep.debian.net/deps/dep8/) automatic tests for packages
that provide them. This means that no extra effort is required to add
new automatic tests to the nightly runs for those packages once they
reach the archive.

### Application manager API to securely enumerate entry points

The `libcanterbury-platform` library now provide APIs for platform
services like the application launcher to enumerate all the installed
applications in a secure fashion and in a UX-agnostic way, without
disclosing such information to unprivileged applications.

The Didcot data sharing service, the Mildenhall application launcher and
status bar services and the Barkway notification service have been
ported to the new API and the old D-Bus API has been dropped (see the
[Breaks](#breaks) section below).

### Multiple mockable backends in geocode-glib

The [work done during the past
release]( {{< ref "/release/16.12/releasenotes.md#multiple-mockable-backends-in-geocode-glib" >}} )
to be able to easily switch backends for the geocoding library
`geocode-glib` has [finally landed
upstream](https://bugzilla.gnome.org/show_bug.cgi?id=756311) and is now
available in the current Apertis release.

## Groundwork

### Track the life cycle of applications and agents using systemd

The Canterbury application manager now creates a transient systemd unit
for all the app-bundles it launches, to be able to reliably track all
the processes created by them. This lays the foundation for a more
advanced life-cycle tracking with better isolation and resource limits.

### Mildenhall UI toolkit

The Mildenhall UI toolkit library has received a lot of attention during
this cycle be easier to use and more reliable. Applications now only
need to include a single `mildenhall/mildenhall.h` header instead of
finding the appropriate header file for every widget used. Applications
also now simply need to link to a single shared object, instead of
per-widget libraries. This is true for the Thornbury utility library as
well, with `thornbury/thornbury.h` and a single shared object.

Mildenhall also saw plenty of fixes to make its code easier to maintain
and more reliable. It is also now possible to test the library without
installing it system-wide, which simplifies development.

### Canterbury streamlining

A lot of effort has been put into simplifying the code in the Canterbury
application manager, making it more reliable and also easier to maintain
and develop. Some unused features have been dropped (see the
[Breaks](#breaks) section) while a wide range of issues from
coding style to memory leaks have been fixed through the whole codebase.

### D-Bus improvements

As preparation for future enhancements to the Canterbury application
manager, some enhancements to the D-Bus interprocess communication
system were written, integrated upstream, and applied to Apertis.

  - AppArmor mediation of D-Bus messages was enhanced to prevent
    services from being launched by applications that would not be
    allowed to send a message to the resulting service, removing an
    opportunity for malicious applications to waste resources.
  - Service activation was enhanced to let Canterbury register new
    activatable services at runtime. This will be used in Apertis 17.06
    to provide standardized freedesktop.org APIs for applications to be
    launched by the app framework, optionally with URIs to open and
    optionally with multiple "views", superseding Apertis-specific
    mechanisms that are [deprecated](#deprecations) due to
    privacy/fingerprinting concerns.

### AppArmor review

Several Apertis-specific AppArmor abstractions were reviewed and
documented, with some of them [deprecated](#deprecations) as
a result of review.

## Design

### Application bundle specification review

The [stable application bundle
specification]( {{< ref "bundle-spec.md" >}} )
has been reviewed thoroughly, leading to several clarifications and the
removal of a few deprecated and no longer used properties.

The application bundle specification has been extended with an official
extension-point mechanism, which is fully compatible with the reference
implementation of AppStream. To support this, the Canterbury application
manager was updated to be compatible with newer versions of the
AppStream library that it uses.

A survey of metadata fields that exist but do not have a specification
or documentation resulted in design tasks , , , , ,  being opened for
future Apertis releases.

### Metadata-based permissions system

Work on the metadata-based permissions system has started, collecting
the initial set of use-cases and comparing Apertis needs to the
solutions provided by the other major platforms (Android, iOS and
[Flatpak](https://blogs.gnome.org/alexl/2017/01/24/the-flatpak-security-model-part-3-the-long-game/)).
This is the first step toward a simpler mechanism to define AppArmor
profiles in a simple way, without having to learn the AppArmor rule
language for the most common cases.

## Deprecations and ABI/API breaks

### Deprecations

During this release cycle we have made our tracking of past and future
API and ABI breaks more systematic by using the [ABI
break](https://phabricator.apertis.org/tag/abi_break/) tag. Many
obsolete or problematic APIs have been deprecated during this release
cycle as a way to clear technical debt in future.

#### Application framework

<dl>

<dt>

: `app-data` and `users` directories are deprecated in favour of
`cby_get_persistence_path()`

<dd>

From the next release Canterbury will stop creating the
`/var/Applications/${bundle_id}/app-data` directory, and the `app-data`
and `users` symlinks in the bundle installation directory in
`/Applications`.

Applications should use the `cby_get_persistence_path()` function to get
the per-bundle, per-user writable data location, or rely on the
freedesktop.org environment variables `XDG_CACHE_HOME`,
`XDG_CONFIG_HOME` and `XDG_DATA_HOME` respectively for temporary data,
configuration data and general data storage.

<dt>

: `play-mode` in command-line arguments is deprecated with no immediate
replacement

<dd>

Canterbury currently has special handling for the command-line arguments
of certain ways to launch entry points. If an argument is exactly
`play-mode` and the app is being launched in "last-user mode", the next
argument is sometimes replaced by `play`, under conditions that are
difficult to describe.

This feature is not sufficiently well-documented to be supported, and is
not compatible with D-Bus activation, which is not argv-based (). If
applications need to know whether they are being launched for last-user
mode, then they should use D-Bus activation, and we should define a key
in the *platform data* dictionary to indicate last-user mode; please
open a task if this is required.

<dt>

: `app-name` and `menu-entry` in command-line arguments are deprecated
with no replacement

<dd>

Canterbury currently has special handling for the command-line arguments
of certain ways to launch entry points. If an argument is named exactly
`app-name`, the next argument is replaced by the entry point ID; if an
argument is named exactly `menu-entry`, the next argument is replaced by
the display name in the form used by the Mildenhall reference UX (in
all-caps with S P A C E S I N S E R T E D).

This feature is not compatible with D-Bus activation, which is not
argv-based (), and is unnecessary now that entry points are always
launched with the same entry point ID and display name (having removed
the special cases that would have used different values for those
arguments in ). It will be removed in a future Canterbury release.

<dt>

: Syntactically invalid entry point IDs are deprecated in favour of
syntactically valid entry point IDs

<dd>

Canterbury currently accepts entry point IDs in store app-bundles that
do not match the syntax in the app-bundle specification. This is
deprecated and will be removed in 17.06. The function
`cby_is_entry_point_id()` can be used to validate entry point IDs. The
syntax rules are identical to D-Bus interface names: see the [Apertis
Application Bundle
Specification]( {{< ref "bundle-spec.md" >}} )
for details.

Canterbury also accepts entry point IDs in built-in app-bundles that do
not match the syntax in the app-bundle specification. This is deprecated
and will be removed in a future Canterbury version. This will require
changing the current special-cases for an app-bundle named exactly
`Launcher` so that instead they apply to an app-bundle named exactly
`org.apertis.Launcher`, or removing those special cases altogether.

<dt>

: Syntactically invalid bundle IDs are deprecated in favour of
syntactically valid bundle IDs

<dd>

Canterbury currently accepts bundle IDs that do not match the syntax in
the app-bundle specification under some circumstances. This is
deprecated and will be removed in 17.06. The function
`cby_is_bundle_id()` can be used to validate bundle IDs. The syntax
rules are identical to D-Bus interface names: see the [Apertis
Application Bundle
Specification]( {{< ref "bundle-spec.md" >}} )
for details.

<dt>

: Canterbury methods, signals, properties that reveal the installed
app-bundles are deprecated

<dd>

In accordance with the security policy documented in Apertis concept
designs, the list of app-bundles installed on the system is considered
to be confidential information. Accordingly, all methods and broadcast
signals that would tell unprivileged app-bundles about other app-bundles
(such as `Launcher.InformAppLaunchStatus` and `AppManager.NewAppState`)
will be removed in a future release. See  for a more comprehensive list.
In particular:

<dl>

<dt>

: The Canterbury AudioMgr interface is deprecated with no immediate
replacement

<dd>

The current AudioMgr interface breaks the app-bundle confidentiality
security requirement and must be changed incompatibly to resolve that.
In the short term, it might be replaced by an incompatible equivalent
that is not considered stable. In the longer term, a new design for
audio management is in progress, with relevant tasks including  and .

<dt>

: `Launcher.CurrentActiveMenuEntry`, `Launcher.SetCurrentCategory`,
`Launcher.InformAppLaunchStatus`, `AppManager.GetGlobalSearchApps` are
deprecated with no replacement

<dd>

`Launcher.InformAppLaunchStatus` is handled by the Mildenhall launcher,
but the handler does not do anything. The others are unused.

<dt>

, : `HardKeys` interface is subject to change

<dd>

,  describe ways in which the interaction between Canterbury and the
Mildenhall Launcher needs to change before  can be resolved. In
addition, the design of this interface could be simplified. This
interface will be changed incompatibly in a future Apertis release.

</dl>

<dt>

: Canterbury methods, signals, properties that use displayed names are
deprecated

<dd>

The name with which an entry point or app-bundle is displayed to the
user is not reliably unique, and any transformations that are applied to
it for a particular UX such as the Mildenhall reference HMI (such as
capitalization changes or inserting S P A C E S) are UX-specific and so
cannot be in a UX-independent component like Canterbury. Methods,
signals and properties like `Launcher.InformAppLaunchStatus` are
deprecated in favour of future APIs that use entry point IDs or bundle
IDs, whichever is more appropriate. They will be removed when a
replacement is available.

<dt>

: `AppDbHandler.LaunchNewApp()` is deprecated

<dd>

This method is redundant with the higher-level API
`cby_entry_point_open_uri_async()` introduced in 17.03, and its only
known user (the Didcot content handover service) has been converted to
use that mechanism. It will be removed in a future Apertis release,
hopefully 17.06, to avoid duplication.

<dt>

: Ribchester's remaining support for GSettings-schema-based entry points
is deprecated in favour of AppStream XML and `CbyComponentIndex`

<dd>

Ribchester has some leftover code for mounting app-bundles that are
represented by GSettings schemas, which was deprecated in 17.03. This
code will be removed in a future Apertis version, hopefully 17.06. Use
AppStream XML to describe application bundles, as described in the
[Apertis Application Bundle
Specification]( {{< ref "bundle-spec.md" >}} ).

</dl>

#### Components

<dl>

<dt>

: Beckfoot is deprecated

<dd>

The Beckfoot network interface management service and its D-Bus and C
APIs are deprecated in favour of direct access to ConnMan (for
management) and GNetworkMonitor (for status querying). It will be
removed in a future Apertis release, hopefully 17.06.

<dt>

: libclapton is deprecated

<dd>

The system information part of libclapton is deprecated, with no direct
replacement. The logging part of libclapton is deprecated in favour of
standard GLib APIs for structured logging. This library will be removed
in a future Apertis release, hopefully 17.06.

<dt>

: libseaton is deprecated

<dd>

The libseaton persistent data management library is deprecated in favour
of direct use of GSettings for preference (configuration) data, or
direct access to appropriate files for other types of data. For
file-based persistence, application authors should choose the most
appropriate file format and directory for the specific data being
stored, using APIs such as `GKeyFile` or `sqlite` to write files in the
`cby_get_persistence_path()`, `g_get_user_data_dir()`,
`g_get_user_cache_dir()` or `g_get_user_config_dir()`. This library will
be removed in a future Apertis release, hopefully 17.06.

<dt>

: Tinwell is deprecated

<dd>

The Tinwell audio playback/recording service and its D-Bus and C APIs
are unmaintained, and are considered deprecated. A new design for audio
management is in progress, with relevant tasks including  and .

</dl>

#### AppArmor

<dl>

<dt>

: `tunables/chaiwala/chaiwala-user` AppArmor tunables are deprecated
with no replacement

<dd>

The `tunables/chaiwala/chaiwala-user` AppArmor profile snippet describes
a filesystem layout that has not existed for some time. It will be
removed in a future Apertis version, hopefully 17.06.

<dt>

: `abstractions/chaiwala-helpers` AppArmor abstraction is deprecated
with no replacement

<dd>

The `abstractions/chaiwala-helpers` AppArmor abstraction has undesirable
security properties and should not be used. It will be removed in a
future Apertis version, hopefully 17.06.

<dt>

: `@{XDGRUNTIMEDIR}` AppArmor variable is deprecated in favour of
`/run/user/*`

<dd>

Apertis' modified AppArmor tunables define a variable named
`@{XDGRUNTIMEDIR}` which is not present in upstream AppArmor, hindering
portability of AppArmor profiles between distributions. It will be
removed in a future Apertis version, hopefully 17.06.

<dt>

: `chaiwala-apparmor-session-lockdown` package is deprecated with no
replacement

<dd>

The purpose of the `chaiwala-apparmor-session-lockdown` package is not
clear; it appears to be a proof-of-concept for our current uses of
AppArmor. It is X11-specific, and therefore is no longer used in our
production (target/development) images, which use Wayland. It will be
removed in a future Apertis version, hopefully 17.06, unless its purpose
is clarified.

<dt>

: `abstractions/dbus-daemon` AppArmor abstraction is deprecated with no
replacement

<dd>

The `abstractions/dbus-daemon` AppArmor abstraction does not appear to
be useful, because every dbus-daemon on Apertis is part of the trusted
computing base and part of the platform infrastructure, so a dbus-daemon
is not a useful thing for third-party code to be launching. It will be
removed in a future Apertis version, hopefully 17.06.

<dt>

: `abstractions/chaiwala-user-write` AppArmor abstraction is deprecated
with no replacement

<dd>

The `abstractions/chaiwala-user-write` AppArmor abstraction is too broad
to be useful, and grants the confined process enough privileges to
escape from its confinement by executing arbitrary unconfined code. It
does not appear to be used in Apertis. It will be removed in a future
Apertis version, hopefully 17.06.

<dt>

: `abstractions/chaiwala-user-read` AppArmor abstraction is deprecated
with no replacement

<dd>

The `abstractions/chaiwala-user-read` AppArmor abstraction is too broad
to be useful, and grants the confined process read access to the entire
home directory. It is a mixture of per-app and non-app-specific paths,
and does not appear to be used in Apertis. It will be removed in a
future Apertis version, hopefully 17.06.

<dt>

: `abstractions/chaiwala-execution` is deprecated in favour of not doing
anything

<dd>

The `abstractions/chaiwala-execution` AppArmor abstraction is empty. It
will be removed in a future Apertis version, hopefully 17.06.

<dt>

: `abstractions/chaiwala-cameras` is deprecated with no immediate
replacement

<dd>

The `abstractions/chaiwala-cameras` AppArmor abstraction does not have
the effect that its documentation suggests. It will be removed in a
future Apertis version, hopefully 17.06.  tracks design work on similar
concepts.

</dl>

#### Mildenhall UI toolkit

<dl>

<dt>

, : Non-namespaced symbols in libmildenhall are deprecated

<dd>

Several widgets and other components in libmildenhall have names that do
not start with the correct [namespace
prefix]( {{< ref "coding_conventions.md#namespacing" >}} ).
All symbols, types and macros not starting with `Mildenhall`,
`mildenhall_`, `MILDENHALL_` are deprecated and will be removed in a
future Apertis version, hopefully 17.06.

<dt>

: Application-specific widgets in libmildenhall are deprecated

<dd>

Many of the widgets in libmildenhall are designed for a particular
application and are not sufficiently general-purpose to be in a library.
All widgets that are not listed in  are deprecated and will be removed
in 17.06.

<dt>

: Including individual libmildenhall headers is deprecated

<dd>

The supported way to include libmildenhall headers is now `#include
<mildenhall/mildenhall.h>`. Including individual headers is deprecated
and may cease to work in a future Apertis version.

<dt>

Including individual libthornbury headers is deprecated

<dd>

The supported way to include libthornbury headers is now `#include
<thornbury/thornbury.h>`. Including individual headers is deprecated and
may cease to work in a future Apertis version.

</dl>

#### Platform libraries

<dl>

<dt>

Including individual libcanterbury-gdbus headers is deprecated

<dd>

The supported way to include libcanterbury-gdbus headers is now
`#include <canterbury/gdbus/canterbury.h>` (although please note that
many of the APIs that this provides are themselves deprecated).
Including individual headers is deprecated and may cease to work in a
future Apertis version. Including `<canterbury.h>` is also deprecated
and may cease to work in a future Apertis version. libcanterbury (check
pkg-config for `canterbury-0`, `#include <canterbury/canterbury.h>`) and
libcanterbury-platform (check pkg-config for `canterbury-platform-0`,
`#include <canterbury/canterbury-platform.h>`) are unaffected.

<dt>

`canterbury` pkg-config module is deprecated

<dd>

The `canterbury.pc` pkg-config module is deprecated and will be removed
in a future Apertis version. Check for `canterbury-0` and include
`<canterbury/canterbury.h>` for general APIs that are intended for all
Apertis components and app-bundles and will be supported long-term.
Check for `canterbury-platform-0` and include
`<canterbury/canterbury-platform.h>` for APIs that require special
privileges, are intended for use by platform components and built-in
app-bundles only, and may change over time. Check for
`canterbury-gdbus-0` and include `<canterbury/gdbus/canterbury.h>` for
D-Bus-oriented APIs that are not long-term stable.

<dt>

Thornbury language API is deprecated in favour of standard gettext APIs

<dd>

`thornbury_get_text()` is deprecated in favour of `gettext()`.
`thornbury_lang_set_current_locale()` is deprecated in favour of
`setlocale()`. `thornbury_lang_configure_AppData()` is deprecated in
favour of `bindtextdomain()`. These functions will be removed in a later
Apertis release.

<dt>

Thornbury parser APIs subject to change

<dd>

`thornbury_parser_parse_prop_file()` and
`thornbury_parser_parse_style_file()` do not use `GError` correctly.
These functions will either be removed or deprecated in a future Apertis
release, in favour of versions that use `GError` correctly.

<dt>

ThornburyModel subject to change

<dd>

`ThornburyModel` and `ClutterListModel` will be replaced by `GListModel`
in a future Apertis release.

<dt>

liblightwood subject to change

<dd>

The lightwood library is in the process of being rewritten, with the
replacement named liblightwood-2. This will lead to an incompatible
change to all libraries that depend on liblightwood, in particular
libmildenhall.

<dt>

Non-policy-compliant package names deprecated

<dd>

Depending or build-depending on the `canterbury-dev` package is
deprecated in favour of `libcanterbury-0-dev` or
`libcanterbury-gdbus-0-dev` or both, whichever is appropriate.
Similarly, depending or build-depending on
`gir1.2-canterbury-platform-0` is deprecated in favour of
`gir1.2-canterburyplatform-0`. This has been done to comply with Debian
best-practice: the new development packages have names that correspond
to the pkg-config module's machine-readable name, and the new
GObject-Introspection package has a name that corresponds to the
typelib's machine-readable name and version. The deprecated transitional
packages will be removed in a future Apertis release.

</dl>

### Breaks

#### Canterbury application manager

<dl>

<dt>

T2707: Canterbury: support for GSettings-schema-based entry points
removed in favour of `.desktop` files

<dd>

The old entry points discovery mechanism based on GSettings schemas,
which was deprecated in 17.03, has been removed. Use `.desktop` files to
describe entry points in application bundles, as described in the
[Apertis Application Bundle
Specification]( {{< ref "bundle-spec.md" >}} ).

<dt>

T3448: Canterbury: APIs to list entry points superseded by
`CbyEntryPointIndex`

<dd>

Older D-Bus APIs to list entry points and create new entry points
without a corresponding `.desktop` file have been removed, including
`AppDbHandler.AppLaunchDb()`, `AppDbHandler.AppLaunchDbUpdate`,
`AppManager.GetApplicationInfo()`, `Launcher.InsertNewEntry`,
`Launcher.RemoveMenuEntry`, `Launcher.PopulateItems()`,
`Launcher.CompletedPopulateItems()`, the whole `PrefMgrManager`
interface, the whole `DataExchangeMgr` interface, the whole `MediaMgr`
interface, `AppManager.InsertNewEntry()`,
`AppManager.SetInstalledApplicationManifest()` and
`AppManager.SetUninstalledApplication()`. Please use
`CbyEntryPointIndex` instead (sample code can be found in the Mildenhall
Launcher). The entry point index automatically monitors store
app-bundles being installed and uninstalled during runtime, and
automatically picks up new platform and built-in entry points at the
next reboot.

<dt>

T3572: Canterbury: Launcher.LaunchNewApp, Launcher.LaunchNewAppResponse
removed in favour of `cby_entry_point_activate_async()`

<dd>

The LaunchNewApp method and its accompanying LaunchNewAppResponse signal
have been removed. Please use `cby_entry_point_activate_async()`
instead.

<dt>

, : Canterbury `DataExchangeMgr` D-Bus interface superseded by
`CbyEntryPointIndex`, `CbyComponentIndex`

<dd>

The `DataExchangeMgr` interface was not used (even by the Didcot content
handover service, which might have been expected to use it) and has been
removed from Canterbury. Its auto-generated C wrappers have been removed
from `libcanterbury-gdbus`. The undocumented
`X-Apertis-DataExchangeRules` and `X-Apertis-ManifestUrl` fields have
also been removed from entry points.

<dt>

, : `MediaMgr` interface removed in favour of possible future work on

<dd>

Older versions of Canterbury contained special cases for removable (USB)
storage media, and for UPnP shares. These special cases were not tested,
and the code released in Apertis 17.03 did not work. Additionally, the
design that had been used was incompatible with . These features have
now been removed. If similar code is required in future, it should be
integrated into the launcher user interface instead of into Canterbury;
tracks possible future work on this topic.

<dt>

: Canterbury integration with "automotive proxy" removed with no
replacement

<dd>

Canterbury's audio manager code (which is itself deprecated) previously
attempted to communicate with an undocumented "automotive proxy"
component, which is not available in Apertis. This has now been removed.

<dt>

: `X-Apertis-WindowName` removed with no immediate replacement

<dd>

Apertis entry points (`.desktop` files) previously had an
`X-Apertis-WindowName` field, which appears to have been used by the
compositor to relate windows to entry points. This mechanism was
insecure (because a malicious program can set any window name of its
choice) and would not have been safe to rely on. It has now been
removed. If there is a need for the compositor to relate windows to
entry points, it must do so via AppArmor labels that cannot be forged
(this is related to the
[Compositor_security]( {{< ref "/compositor_security.md" >}} ) topic).

<dt>

: `X-Apertis-MimeList`, `mt_app_settings`, `mt_browse` superseded by
standard `MimeType`, `Interfaces`

<dd>

Apertis entry points (`.desktop` files) previously had an
`X-Apertis-MimeList` field, which was used in older versions of the
Didcot content-handover service. This was confirmed to be obsolete, and
has been removed. The `mt_app_settings` pseudo-interface and the
`mt_browse` pseudo-content-type were also removed.

<dt>

Canterbury SONAME change

<dd>

The ABI version suffix of `libcanterbury-gdbus` was changed to reflect
the fact that ABIs were removed, and small libraries for each Canterbury
D-Bus interface (such as `libcanterburyappdbhandler.so.0`) were removed.
Applications that use the remaining D-Bus APIs must be recompiled
against Canterbury 0.1703.x. The `canterbury-0` and
`canterbury-platform-0` libraries are unaffected.

</dl>

#### Other components

<dl>

<dt>

: Chalgrove, Corbridge removed

<dd>

The Chalgrove preferences management service and all its D-Bus and C
APIs have been removed in favour of the standard GSettings API. The
Corbridge Bluetooth management service and all its D-Bus and C APIs have
been removed in favour of direct use of the ConnMan and BlueZ APIs.

<dt>

: Webkit-Clutter—based web view widget superseded by `libbredon`

<dd>

As part of an effort to replace Webkit-Clutter with Webkit2-GTK+, the
`LightwoodWebViewWidget` has been removed from `liblightwood`. Please
use `libbredon` instead.

<dt>

Didcot Share interface was removed with no replacement.

<dd>

The `org.apertis.Didcot.Share` interface in 17.03 and earlier versions
did not work. It is no longer present in 17.03.

<dt>

Didcot SONAME change

<dd>

The ABI version suffix of `libdidcot-0` was changed to reflect the fact
that ABIs were removed, and the compatibility symbolic links for the
older libraries `libdidcotlaunchiface` and `libdidcotshareiface` were
removed. Applications that use the remaining D-Bus APIs must be
recompiled against Didcot 0.1703.x.

</dl>

## Infrastructure

### Apertis infrastructure tools

For Debian Jessie based systems:

` $ deb `<https://repositories.apertis.org/debian/>` jessie tools`

For Ubuntu Trusty based systems:

` $ deb `<https://repositories.apertis.org/ubuntu/>` trusty tools`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

## Known issues

### High (8)

  - Focus issue with Audio player Thumb view

  - Info-roller Header is overlapping with roller texts in Songs
    application for detailed view

  - Playing videos crashes the web process (inside GStreamer)

  - denial of service via removable media with cloned btrfs UUIDs,
    partition labels, etc.

  - ade: hello-world application cross build fails

  - Cross debugging through GDB and GDBserver Not possible.

  - A video plays in a partial window in full screen mode

  - Application Installed through ADE is not listed in launcher

### Normal (114)

  - Not able to load heavy sites on GtkClutterLauncher

  - No connectivity Popup is not seen when the internet is disconnected.

  - remove INSTALL, aclocal.m4 files from langtoft

  - Documentation is not available from the main folder

  - Power button appers to be disabled on target

  - apparmor-pulseaudio: ARM Failed to drain stream: Timeout

  - apparmor-libreoffice: libreoffice.normal.expected fails:
    ods_to_pdf: fail

  - Network search pop-up isn't coming up in wi-fi settings

  - mxkineticscrollview-smooth-pan:Free scroll doesn't work

  - factory-reset-tool TC: flagcheck messages are hidden by Plymouth

  - Screen flickering Issue

  - Clutter_text_set_text API redraws entire clutterstage

  - libgrassmoor: executes tracker-control binary

  - mildenhall-settings: does not generate localization files from
    source

  - Videos are hidden when Eye is launched

  - cgroups-resource-control: blkio-weights tests failed

  - Theme ,any F node which is a child of an E node is not working for
    Apertis widgets.

  - Video doesn't play when toggling from full screen to detail view

  - Bluetooth pairing option not working as expected

  - Target doesn't reboot after system update

  - Back option is missing as part of the tool tip

  - Unsupported launguage text is not shown on the page in
    GtkClutterLauncher

  - minimal image: DISPLAY sanity check tests failed

  - apt-key fails to access the Apertis keyring when ran from apt-get
    from a root login shell

  - Investigate why development ARM image fails to generate

  - Thornbury model functions deal with instances of the wrong class

  - telepathy-gabble: Several tests failed

  - apparmor-session-lockdown-no-deny asserts that canterbury is
    running, but that isn't true on SDK

  - Cookies cannot be activated in the browser

  - Youtube videos cannot be viewed on browser

  - Horizontal scroll is not shown on GtkClutterLauncher

  - libsoup-unit: ssl-test failed for ARM

  - The background HMI is blank on clicking the button for Power OFF

  - Only half of the hmi is covered when opening gnome.org

  - Share links to facebook, twitter are nbt working in browser
    (GtkClutterLauncher)

  - Background video is not played in some website with
    GtkClutterLauncher

  - newport-client unit test relies on third party servers

  - Interaction with PulseAudio not allowed by its AppArmor profile

  - Fix folks EDS tests to not be racy

  - GStreamer playbin prioritises imxeglvivsink over
    clutterautovideosink

  - factory reset with a different image's rootfs.tar.gz results in
    emergency mode

  - shapwick reads /etc/nsswitch.conf and /etc/passwd, and writes
    /var/root/.cache/dconf/

  - LAVA: Could not resolve host: repositories.apertis.org

  - gupnp-services: test service failed

  - Drop down lists are not working on a site like facebook

  - Cannot open/view pdf documents in browser (GtkClutterLauncher)

  - Zoom in feature does not work on google maps

  - Crash when initialising egl on ARM target

  - Video doesn't resize properly in fullscreen

  - Images for the video links are not shown in news.google.com on
    GtkClutterLauncher

  - Pulse Audio volume control doesn't launch as a separate window on
    SDK

  - Mismatch between server version file and sysroot version

  - Focus in launcher rollers broken because of copy/paste errors

  - Test apps are failing in Liblightwood with the use of GTest

  - If 2 drawers are activated, the most recent one hides behind the
    older one, instead of coming on top of older one.

  - Album Art Thumbnail missing in Thumb view in ARTIST Application

  - tracker-indexing-local-storage: Stopping tracker-store services

  - apparmor-tracker: underlying_tests failed

  - Canterbury messes up kerning when .desktop uses unicode chars

  - Ribchester: deadlock when calling RemoveApp() right after RollBack()

  - make check fails on libbredon package for wayland warnings

  - polkit-parsing: TEST_RESULT:fail

  - Not enough space in /opt to download and install sysroot

  - mildenhall_launcher process needs to be killed in order to view
    browser using webkit2 GtkClutterLauncher

  - Cannot open links within website like yahoo.com

  - tracker tests: Error creating thumbnails: No poster key found in
    metadata

  - apparmor-folks: unable to link contacts to test unlinking

  - mx6qsabrelite: linking issue with libgstimxeglvivsink.so and
    libgstimxvpu.so gstreamer plugins

  - libgles2-vivante-dev is not installable

  - telepathy-gabble-tests should depend on python-dbus

  - Compositor seems to hide the bottom menu of a webpage

  - Content on a webpage doesn't load in sync with the scroll bar

  - Resizing the window causes page corruption

  - gupnp-services tests test_service_browsing and
    test_service_introspection fail on target-arm image

  - Confirm dialog status updated before selecting the confirm option
    YES/NO

  - webview Y offset not considered to place, full screen video on
    youtube webpage

  - cgroups-resource-control: test network-cgroup-prio-class failed

  - folks: random tests fail

  - Album art is missing in one of the rows of the songs application

  - Canterbury entry-point launching hides global popups, but only
    sometimes

  - <abstractions/cameras> doesn't seem to do what it is intended to do

  - /usr/bin/gst-launch-1.0, gst-inspect-1.0 etc. profiles should
    probably not exist

  - <abstractions/chaiwala-base> gives privileges that not every
    app-bundle should have

  - <abstractions/chaiwala-execution> does nothing

  - <abstractions/chaiwala-user-read> should be removed

  - <abstractions/chaiwala-user-write> should be removed

  - <abstractions/dbus-daemon> should be removed

  - mildenhall-popup-layer fails to build on Jenkins

  - Apertis apparmor package defines non-upstream XDGRUNTIMEDIR variable

  - <abstractions/chaiwala-helpers> should be removed

  - <tunables/chaiwala/chaiwala-user> should be removed

  - <abstractions/chaiwala-media> contains test-specific rules

  - Segmentation fault when disposing test executable of mildenhall

  - The web runtime doesn't set the related view when opening new
    windows

  - Deploying LAVA tests for SDK images on personal stream fails

  - Voice/Audio is not heard for the Bluez-hfp profile in i.MX6

  - Make maintainer-clean fails

  - GtkClutterLauncher: Segfaults with mouse right-clicking

  - didcot-client: Unit didcot.service could not be found.

  - rhosydd: integration test fails

  - ribchester: gnome-desktop-testing test times out

  - canterbury: Most of the tests fail

  - didcot does not show popup with application list, when client
    queries OpenURI method to open a URI(which can be handled by
    multiple applications)

  - Status bar is not getting updated with the current song/video being
    played

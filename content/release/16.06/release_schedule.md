+++
date = "2016-07-01"
weight = 100

title = "16.06 Release schedule"

aliases = [
    "/old-wiki/16.06/Release_schedule"
]
+++

The 16.06 release cycle started in March 2016. [You can follow the
countdown on Phabricator](https://phabricator.apertis.org/countdown/).

<table>
<thead>
<tr class="header">
<th><p>Milestone</p></th>
<th><p>Date</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Start of release cycle</p></td>
<td><p>2016-03-19</p></td>
</tr>
<tr class="even">
<td><p>Soft feature freeze: end of feature proposal and review period</p></td>
<td><p><del>2016-06-01</del><br />
2016-06-29</p></td>
</tr>
<tr class="odd">
<td><p>Hard feature freeze: end of feature development for this release</p></td>
<td><p><del>2016-06-08</del><br />
2016-07-06</p></td>
</tr>
<tr class="even">
<td><p>Release candidate 1/hard code freeze: no new code changes may be made after this date</p></td>
<td><p><del>2016-06-21</del><br />
2016-07-19</p></td>
</tr>
<tr class="odd">
<td><p>16.06 release</p></td>
<td><p><del>2016-06-24</del><br />
2016-07-22</p></td>
</tr>
</tbody>
</table>

## See also

  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and
    information

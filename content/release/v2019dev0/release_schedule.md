+++
date = "2019-04-05"
weight = 100

title = "V2019dev0 Release schedule"

aliases = [
    "/old-wiki/V2019dev0/Release_schedule",
    "/old-wiki/19.03/Release_schedule"
]
+++

The v2019dev0 release cycle started in January 2019.

<table>
<thead>
<tr class="header">
<th><p>Milestone</p></th>
<th><p>Date</p></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td><p>Start of release cycle</p></td>
<td><p>2019-01-01</p></td>
</tr>
<tr class="even">
<td><p>Soft feature freeze: end of feature proposal and review period</p></td>
<td><p><s>2019-02-19</s><br />
2019-03-22</p></td>
</tr>
<tr class="odd">
<td><p>Soft code freeze/hard feature freeze: end of feature development for this release, only bugfixes allowed</p></td>
<td><p><s>2019-02-26</s><br />
2019-03-29</p></td>
</tr>
<tr class="even">
<td><p>Release candidate 1 (RC1)/hard code freeze: no new code changes may be made after this date</p></td>
<td><p><s>2019-03-12 EOD</s><br />
2019-04-05 EOD</p></td>
</tr>
<tr class="odd">
<td><p>RC testing</p></td>
<td><p><s>2019-03-13/2019-03-17</s><br />
2019-04-09..11</p></td>
</tr>
<tr class="even">
<td><p>v2019dev0 release</p></td>
<td><p><s>2019-03-18</s><br />
2019-04-12</p></td>
</tr>
</tbody>
</table>

If the release candidate 1 does not successfully pass all required
tests, then the issues will be fixed and a new release candidate will be
re-tested. This would delay the release, which would be reflected on
this page.

## See also

  - Previous [release schedules]( {{< ref "/releases.md" >}} ) and more
    information about the timeline

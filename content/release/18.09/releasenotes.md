+++
date = "2019-10-25"
weight = 100

title = "18.09 ReleaseNotes"

aliases = [
    "/old-wiki/18.09/ReleaseNotes"
]
status = "Deprecated"
statusDescription = "This release of Apertis has been deprecated. Links to images and other resources have either been removed or are likely to be dead."
+++

# Apertis 18.09 Release

**18.09** is the September 2018 stable development release of
**Apertis**, a Debian/Ubuntu derivative distribution geared towards the
creation of product-specific images for ARM (both the 32bit ARMv7 and
64-bit ARMv8 versions using the hardfloat ABI) and Intel x86-64 (64-bit)
systems.

This Apertis release is based on top of the Ubuntu 16.04 (Xenial) LTS
release with several customizations. Test results of the 18.09 release
are available in the [18.09 test report]().

### Release downloads

| [Apertis 18.09 images]() |
| ----------------------------------------------------------------- |
| Intel 64-bit                                                      |
| ARM 32-bit (U-Boot)                                               |
| ARM 32-bit (MX6q Sabrelite)                                       |
| ARM 64-bit (U-Boot)                                               |

The Intel `minimal`, `target` and `development` images are tested on the
[reference hardware (MinnowBoard MAX)]( {{< ref "/reference_hardware/_index.md" >}} ),
but they can run on any UEFI-based x86-64 system. The `sdk` image is
[tested under VirtualBox]( {{< ref "/virtualbox.md" >}} ).

#### Apertis 18.09 repositories

` $ deb `<https://repositories.apertis.org/apertis/>` 18.09 target helper-libs development sdk hmi`

## New features

### Testing on immutable rootfs

The value of tests directly depends on how the system being tested
matches what is ultimately shipped. Ideally those should match 100%, but
tests themselves need some infrastructure, helpers, libraries or other
kind of dependencies around them. So far the Apertis testsuite required
the installation of a large set of extra packages to satisfy all the
needs of the many tests that are run, but this greatly reduced the value
of the tests themselves as the tested system diverged significantly from
the original image.

With the introduction in the recent releases of images with immutable
rootfs based on [OSTree]( {{< ref "/guides/ostree.md" >}} ) the approach above was
entirely not viable as it relies on mutating the base rootfs.

A set of high priority tests has now been converted to be self-contained
and not require the installation of any additional package. These tests
run on modifiable APT-based images or on OSTree-based images with an
immutable rootfs with no changes. A new [set of
helpers](https://gitlab.apertis.org/tests/helper-tools/) with the help
of Jenkins automation let test writers bundle each test and its
dependencies into a single archive that is deployed on the system under
tests by LAVA in `/var/lib/tests`.

A new page documents [how to write self-contained tests using bundled
dependencies on a immutable rootfs]( {{< ref "/qa/immutablerootfstests.md" >}} )
and the [Test Cases
Guidelines]( {{< ref "/qa/test_cases_guidelines.md#download-self-contained-test-data" >}} )
have been updated.

### GPLv3 sources cleanup

Apertis strives to provide a core featureset that does not contain
GPLv3-licensed code, to give more flexibility to adopters. However,
while the produced binaries are not covered by the GPLv3, some packages
carry GPLv3 code in their source for their build scripts, tests,
debugging utilities or example programs. This is not an issue since no
GPLv3 code ends up in the artifacts, but some packages have been
reworked to avoid GPLv3-covered code in their source to simplify
licensing compliance checks.

### Full file and package lists for each artifact

Each ospack and image produced by our pipeline is now accompanied by
listing of all the packages and files shipped by them, with a wide set
of metadata. This is useful to quickly check the contents of each
artifact without downloading and extracting multiple gigabytes of data,
in particular when doing checks across multiple architectures and
versions. The provided metadata can also be used to check which image is
affected by a specific issue or to collect statistics about the artifact
contents.

## Build and integration

### Automatic updates on Phabricator about test failures in LAVA

LAVA test failures now automatically trigger the creation of tasks in
Phabricator thanks to the [LAVA – Phab
bridge](https://gitlab.apertis.org/infrastructure/lava-phab-bridge).
Subsequent runs of the failed test results in the same Phabricator task
being updated with errors and successes. Developers are thus kept up to
date with the latest state of any issue with no extra effort.

### Dockerization of the package-building Jenkins jobs

The Apertis image building pipeline already runs using Docker as its
environment, but older Jenkins jobs like the ones fetching changes from
git and uploading package sources to OBS were executed directly on a
specifically configured host.

This had several drawbacks, such as the inability to properly support
different releases and conflicting dependencies. To overcome those
issues the [packaging jobs are now fully
dockerized](https://gitlab.apertis.org/infrastructure/apertis-jenkins-jobs/commit/59af079a3b),
using a [dedicated Apertis Docker
image](https://gitlab.apertis.org/infrastructure/apertis-docker-images/-/tree/18.09/apertis-package-builder)
as their environment, bringing several advantages:

  - each job runs in a separate container, giving more control of
    resource usage
  - Docker containers are instantiated automatically by Jenkins
  - rebuilding Docker containers from scratch to get the latest updates
    can be done with a single click
  - the containers provide a reproducible environment across workers
  - Docker container images are built from Dockerfiles controlled by
    developers using the normal review workflow with no special
    privileges
  - the same container images used on the Jenkins workers can be used to
    reproduce the work environment on developers' machines
  - containers are ephemeral, a job changing the work environment does
    not affect other jobs nor subsequent runs of the same job
  - containers are isolated from each other and allow to address
    conflicting requirements using different images
  - several service providers offer Docker support which can be used for
    scaling

In particular the dockerization also makes things easier for
downstreams, which now no longer need to manually administrate a custom
worker machine and instead can easily and reliably replicate the
environment used by Apertis.

### Dockerization of the Jenkins jobs to support Phabricator

Like the packaging jobs, the [jobs checking and build-testing patches
submitted to
Phabricator](https://gitlab.apertis.org/infrastructure/apertis-jenkins-jobs/commit/60eb971fc0)
are now running in the same Apertis Docker environment. Even if the plan
is to switch the [contribution
process]( {{< ref "contributions.md" >}} )
to a workflow based on [GitLab Merge
Requests](https://docs.gitlab.com/ce/user/project/merge_requests/) the
conversion of the Phabricator jobs lays the groundwork for enabling
GitLab CI checks in the near future.

### Improved customizability of Jenkins job templates

The Apertis job templates necessarily hardcode settings that are
specific to our infrastructure, but the recent work to improve the
customizability of [git
credentials](https://gitlab.apertis.org/infrastructure/apertis-jenkins-jobs/commit/6614b11c93)
and [OBS
credentials](https://gitlab.apertis.org/infrastructure/apertis-jenkins-jobs/commit/3020d0b7d9)
makes changing such settings simpler and easier to maintain for any
interested downstream.

## Design and documentation

### Sensors and Actuators: Vehicle API status analysis

The [Sensors and
Actuators]( {{< ref "sensors-and-actuators.md" >}} )
document has been extended to describe the [current status of the
API]( {{< ref "sensors-and-actuators.md#sensors-and-actuators-api" >}} )
implemented by [Rhosydd](https://gitlab.apertis.org/appfw/rhosydd/).

The evolution of the W3C Vehicle API specifications from the [Vehicle
Data specification](http://www.w3.org/2014/automotive/data_spec.html)
and [Vehicle Information Access
API](http://www.w3.org/2014/automotive/vehicle_spec.html) to the
[Vehicle Signal
Specification](https://github.com/GENIVI/vehicle_signal_specification)
and [Vehicle Information Service
Specification](https://www.w3.org/TR/vehicle-information-service/) has
been considered, analyzing the impact that it would have on the Apertis
API.

### System updates and rollback: Expand OSTree topics

The [System updates and
rollback]( {{< ref "system-updates-and-rollback.md" >}} )
document has seen more coverage of OSTree-related topics, such as the
guarantee that either an update succeeds or that the previous
configuration stays available with no changes, the security mechanisms
provided to ensure the integrity and appropriateness of updates and
other considerations about offline updates over USB mass storage device
using OSTree static deltas.

## Deprecations and ABI/API breaks

### Deprecations

During this release cycle we have continued to mark obsolete or
problematic APIs with the [ABI
break](https://phabricator.apertis.org/tag/abi_break/) tag as a way to
clear technical debt in future.

#### Phabricator code contribution workflow

<dl>

<dt>

The use of Phabricator Differential and `git-phab` for code
contributions and reviews is deprecated in favour of the GitLab merge
request workflow

<dd>

During the next release Apertis will fully switch to use the GitLab
merge request workflow for all contributions, which is more integrated
and smoother than the Phabricator Differential workflow. The
documentation on the wiki will be updated during the upcoming cycle.

</dl>

#### Development images

<dl>

<dt>

The `development` image type is deprecated in favour of extending the
`target` image type.

<dd>

From the next release Apertis will no longer ship `development` images.
Users can pick the `target` images and manually install any needed
package with `apt`. The inclusion of any specific tool to be shipped
out-of-the-box on the `target` can be requested on Phabricator.

</dl>

#### Mildenhall UI framework

<dl>

<dt>

The Mildenhall UI toolkit is deprecated in favour of other UI toolkits
such as GTK+, Qt, or HTML5-based solutions

<dd>

From the next release Apertis may no longer ship the Mildenhall UI
toolkit nor any accompanying libraries, as the underlying Clutter
library has been deprecated upstream for some years now.

</dl>

#### Application framework

<dl>

<dt>

`ribchester-full` and `canterbury-full` are deprecated

<dd>

From the next release Canterbury and Ribchester will only offer the APIs
provided by the respective `-core` and the additional APIs offered by
the `-full` packages will be dropped

</dl>

### Breaks

<dl>

<dt>

The experimental `GtkApertisStylable` API/ABI exported by `libgtk-3.so`
has been dropped

<dd>

Due to upstream changes in GTK3 the `GtkApertisStylable` patches caused
severe regressions in every GTK3 application such as Meld. For this
reason the `GtkApertisStylable` API has been dropped in the previous
cycle (18.06) but was not mentioned in the release notes.

</dl>

## Infrastructure

### Apertis infrastructure tools

The Apertis Debian Stretch tools repositoriy provides packages for
`git-phab`, `lqa`, Debos and deps, LAVA v2, OBS 2.7, `ostree-push` and
`jenkins-job-builder`:

` $ deb `<https://repositories.apertis.org/debian/>` stretch tools`

### Images

Image daily builds, as well as release builds can be found at:

` `<https://images.apertis.org/>

Image build tools can be found in the Apertis tools repositories.

### Infrastructure overview

The [Image build infrastructure
document]( {{< ref "image-build-infrastructure.md" >}} )
provides an overview of the image building process and the involved
services.

## Known issues

### High (1)

  - OSTree fails to build working ostree images in SDK

### Normal (61)

  - connman shows incorrect Powered status for the bluetooth technology

  - property changed signal in org.bluez.Network1 is not emiting when
    PAN disconnects

  - cgroups-resource-control: blkio-weights tests failed

  - libsoup-unit: ssl-test failed for ARM

  - Fix folks EDS tests to not be racy

  - GStreamer playbin prioritises imxeglvivsink over
    clutterautovideosink

  - gupnp-services: test service failed

  - Crash when initialising egl on ARM target

  - Pulse Audio volume control doesn't launch as a separate window on
    SDK

  - Test apps are failing in Liblightwood with the use of GTest

  - apparmor-tracker: underlying_tests failed

  - VirtualBox display freezes when creating multiple notifications at
    once and interacting (hover and click) with them

  - Ribchester: deadlock when calling RemoveApp() right after RollBack()

  - tracker tests: Error creating thumbnails: No poster key found in
    metadata

  - apparmor-folks: unable to link contacts to test unlinking

  - mx6qsabrelite: linking issue with libgstimxeglvivsink.so and
    libgstimxvpu.so gstreamer plugins

  - libgles2-vivante-dev is not installable

  - gupnp-services tests test_service_browsing and
    test_service_introspection fail on target-arm image

  - folks: random tests fail

  - arm-linux-gnueabihf-pkg-config does not work with sysroots installed
    by \`ade\`

  - Mismatching gvfs/gvfs-common and libatk1.0-0/libatk1.0-data package
    versions in the archive

  - Observing multiple service instances in all 17.06 SDK images

  - Containers fail to load on Gen4 host

  - gnutls depends on old abandoned package gmp4 due to licensing
    reasons

  - Preseed action is needed for Debos

  - Debos: allow to use partition number for 'raw' action

  - Debos: mountpoints which are mounted only in build time

  - Debos: option for kernel command parameters for 'filesystem-deplay'
    action

  - Cancel/Cancelling issue while running test jobs on LAVA

  - Unable to boot apertis/ecore images on rcar boards for LAVA

  - ribchester-core causes apparmor denies on non-btrfs minimal image

  - Debos: changes 'resolv.conf' during commands execution chroot

  - Volume Control application hangs on opening it

  - gupnp-services: browsing and introspection tests fail

  - sqlite: Many tests fail for target amd64, armhf and sdk

  - dbus-installed-tests: service failed because a timeout was exceeded

  - boot-no-crashes: ANOM_ABEND found in journalctl logs

  - tracker-indexing-mass-storage test case fails

  - do-branching fails at a late stage cloning OBS binary repos

  - A 2-3 second lag between the speakers is observed when a hfp
    connection is made over bluetooth

  - ade process fails to start gdbserver if a first try has failed

  - SDK hangs when trying to execute bluez-hfp testcase

  - OSTree testsuite hangs the gouda Jenkins build slave

  - Couldn't install application: error seen when trying to install to
    target using ade

  - Ribchester mount unit depends on Btrfs

  - Eclipse Build is not working for HelloWorld App

  - Not able to create namespace for AppArmor container on
    \`mx6qsabrelite\` board

  - Seek/Pause option does not work correctly on YouTube

  - Failed unmounting /var bind mount error seen on shutting down an
    i.MX6/Minnowboard with Minimal ARM-32/AMD64 image

  - Apps like songs, artists etc are not offser correctly by the
    compositor window

  - Double tapping on any empty space in the songs app's screen results
    in the compositor window hinding the left ribbon

  - Back button is hidden by the compositor window in apps like songs,
    artists etc

  - Ensure that the uploaded linux package version uniquely identifies a
    single commit

  - Canterbury needs to explicitly killed to relauch the Mildenhall
    compositor

  - Mildenhall compositor gets offset when we open an app like songs

  - kernel panic occurs when trying to boot an image on i.mx6 revision
    1.2 board from LAVA

  - build failure in liblightwood-2 on 18.06 image

  - Back button is missing in apps like songs

  - "Setup Install to Target" option is not showing on eclipse

  - Ospacks ship a .gitignore file in the root directory

  - Ospacks ship an empty /script directory in the root directory

  - recipe for target 'install' failed error seen on executing the
    install to target using ade

  - Pause button is missing in the songs app in OSTree based images

### Low (111)

  - Remove unnecessary folks package dependencies for automated tests

  - Not able to load heavy sites on GtkClutterLauncher

  - No connectivity Popup is not seen when the internet is disconnected.

  - Upstream: linux-tools-generic should depend on lsb-release

  - remove INSTALL, aclocal.m4 files from langtoft

  - Mildenhall compositor crops windows

  - Documentation is not available from the main folder

  - Power button appers to be disabled on target

  - apparmor-libreoffice: libreoffice.normal.expected fails:
    ods_to_pdf: fail \[Bugzilla bug \#331\]

  - Network search pop-up isn't coming up in wi-fi settings

  - Clutter_text_set_text API redraws entire clutterstage

  - libgrassmoor: executes tracker-control binary

  - mildenhall-settings: does not generate localization files from
    source

  - Videos are hidden when Eye is launched

  - Theme ,any F node which is a child of an E node is not working for
    Apertis widgets.

  - Video doesn't play when toggling from full screen to detail view

  - Simulator screen is not in center but left aligned

  - Back option is missing as part of the tool tip

  - Unsupported launguage text is not shown on the page in
    GtkClutterLauncher

  - The video player window is split into 2 frames in default view

  - Horizontal scroll is not shown on GtkClutterLauncher

  - The background HMI is blank on clicking the button for Power OFF

  - Only half of the hmi is covered when opening gnome.org

  - Share links to facebook, tw

  - Cancel/Cancelling issue while running test jobs on LAVA

  - Unable to boot apertis/ecore images on rcar boards for LAVAitter are
    nbt working in browser (GtkClutterLauncher)

  - Background video is not played in some website with
    GtkClutterLauncher

  - Interaction with PulseAudio not allowed by its AppArmor profile

  - shapwick reads /etc/nsswitch.conf and /etc/passwd, and writes
    /var/root/.cache/dconf/

  - Cannot open/view pdf documents in browser (GtkClutterLauncher)

  - Zoom in feature does not work on google maps

  - Printing hotdoc webpage directly results in misformatted document

  - Images for the video links are not shown in news.google.com on
    GtkClutterLauncher

  - Focus in launcher rollers broken because of copy/paste errors

  - beep audio decoder gives errors continously

  - If 2 drawers are activated, the most recent one hides behind the
    older one, instead of coming on top of older one.

  - Album Art Thumbnail missing in Thumb view in ARTIST Application

  - Unusable header in Traprain

  - Cancel/Cancelling issue while running test jobs on LAVA

  - Unable to boot apertis/ecore images on rcar boards for LAVA section
    in Devhelp

  - Clang package fails to install appropriate egg-info needed by hotdoc

  - gstreamer1-0-decode: Failed to load plugin warning

  - Canterbury messes up kerning when .desktop uses unicode chars

  - make check fails on libbredon package for wayland warnings

  - Cannot open links within website like yahoo.com

  - Compositor seems to hide the bottom menu of a webpage

  - Spacing issues between text and selection box in website like amazon

  - Content on a webpage doesn't load in sync with the scroll bar

  - Resizing the window causes page corruption

  - Confirm dialog status updated before selecting the confirm option
    YES/NO

  - webview Y offset not considered to place, full screen video on
    youtube webpage

  - cgroups-resource-control: test network-cgroup-prio-class failed

  - GObject Generator link throws 404 error

  - GLib, GIO Reference Manual links are incorrectly swapped

  - Album art is missing in one of the rows of the songs application

  - Canterbury entry-point launching hides global popups, but only
    sometimes

  - <abstractions/chaiwala-base> gives privileges that not every
    app-bundle should have

  - Segmentation fault when disposing test executable of mildenhall

  - The web runtime doesn't set the related view when opening new
    windows

  - GtkClutterLauncher: Segfaults with mouse right-clicking

  - ribchester: gnome-desktop-testing test times out

  - canterbury: Most of the tests fail

  - Status bar is not getting updated with the current song/video being
    played

  - Compositor hides the other screens

  - Songs do not start playing from the beginning but instead start a
    little ahead

  - Roller problem in settings application

  - Variable roller is not working

  - In mildenhall, URL history speller implementation is incomplete.

  - MildenhallSelectionPopupItem doesn't take ownership when set
    properties

  - libshoreham packaging bugs

  - libmildenhall-0-0 contains files that would conflict with a future
    libmildenhall-0-1

  - rhosydd-client crashes when displaying vehicle properties for mock
    backend

  - Rhosydd service crashes when client exits on some special usecases
    (Refer description for it)

  - MildenhallSelPopupItem model should be changed to accept only gchar
    \* instead of MildenhallSelPopupItemIconDetail for icons

  - libbredon/seed uninstallable on target as they depend on libraries
    in :development

  - webview-test should be shipped in libbredon-0-tests instead of
    libbredon-0-1

  - bredon-0-launcher should be shipped in its own package, not in
    libbredon-0-1

  - virtual keyboard is not showing for password input field of any
    webpage

  - Steps like pattern is seen in the background in songs application

  - Avoid unconstrained dbus AppArmor rules in frome

  - Newport test fails on minimal images

  - "connman: patch ""Use ProtectSystem=true"" rejected upstream"

  - "connman: patch ""device: Don't report EALREADY"" not accepted
    upstream"

  - webkit2GTK crash observed flicking on webview from other widget

  - Mildenhall should install themes in the standard xdg data dirs

  - Page rendering is not smooth in sites like www.yahoo.com

  - HTML5 demo video's appear flipped when played on webkit2 based
    browser app

  - Render theme buttons are not updating with respect to different zoom
    levels

  - Rendering issue observed on websites like
    <http://www.moneycontrol.com>

  - Frampton application doesn't load when we re-launch them after
    clicking the back button

  - Crash observed on webruntime framework

  - Crash observed on seed module when we accesing the D-Bus call method

  - introspectable support for GObject property, signal and methods

  - On multiple re-entries from settings to eye the compositor hangs

  - Segmentation fault occurs while exectuing webkit2gtk-aligned-scroll
    test case

  - zoom feature is not working as expected

  - Segmentation fault is observed on closing the mildenhall-compositor

  - Building cargo and rustc in 17.12 for Firefox Quantum 57.0

  - Blank screen seen on executing last-resort-message testcase

  - Inital Roller mappings are misaligned on the HMI

  - folks-metacontacts: folks-metacontacts-linking_sh.service failed

  - folks-telepathy: folks-retrieve-contacts-telepathy_sh.service
    failed

  - folks-alias-persistence: folks-alias-persistence_sh.service failed

  - folks-metacontacts-antilinking:
    folks-metacontacts-antilinking_sh.service failed

  - folks-search-contacts: folks-search-contacts_sh.service failed

  - apparmor-tracker: AssertionError: False is not true

  - apparmor-folks: Several tests fail

  - apparmor-gstreamer1.0: gstreamer1.0-decode: assertion failed

  - tracker-indexing-local-storage: AssertionError: False is not true

  - didcot: test_open_uri: assertion failed

  - traprain: sadt: error: cannot find debian/tests/control

  - canterbury: core-as-root and full-as-root tests failed

  - ribchester: Job for generated-test-case-ribchester.service canceled

  - apparmor-pulseaudio: pulseaudio.normal.expected_underlying_tests
    fails

  - webkit2gtk-drag-and-drop doesn't work with touch
